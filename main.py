import config
import sqlite3
from mastodon import Mastodon
import time
from datetime import datetime, timedelta
from dateutil import parser
from nltk.tokenize import word_tokenize
from nltk.metrics import jaccard_distance
import feedparser

mastodon = Mastodon(
    access_token = config.auth_token,
    api_base_url = config.api_base_url
)

def compare_articles(title1, title2):
    # Tokenize the titles into sets of words
    tokens1 = set(word_tokenize(title1.lower()))
    tokens2 = set(word_tokenize(title2.lower()))
    
    # Calculate the Jaccard distance
    distance = jaccard_distance(tokens1, tokens2)
    print(distance)
    
    # Threshold the distance to determine similarity
    if distance <= 0.5:
        return True
    else:
        return False

def read_rss_links(filename):
    with open(filename, 'r') as file:
        return [line.strip() for line in file]

def fetch_articles(rss_link):
    feed = feedparser.parse(rss_link)
    return [(entry.title, entry.link, entry.published, feed.feed.title) for entry in feed.entries]

def format_date(date_str):
    try:
        date = parser.parse(date_str)
        return date.strftime('%Y-%m-%d %H:%M:%S')
    except ValueError:
        return None

def save_articles(articles, conn):
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS articles (title text, link text, date text, source text, shared integer)''')
    for title, link, date, source in articles:
        date = format_date(date)

        c.execute("""INSERT INTO articles (title, link, date, source, shared)
                     SELECT ?, ?, ?, ?, 0
                     WHERE NOT EXISTS (SELECT 1 FROM articles WHERE title = ?)""",
                  (title, link, date, source, title))
    conn.commit()

def share_articles():
    # Connect to the database
    conn = sqlite3.connect('articles.db')
    c = conn.cursor()

    # Get all the articles that haven't been shared
    c.execute("SELECT title, link, source, date FROM articles WHERE shared=0")
    articles = c.fetchall()

    # Iterate over the articles
    for article in articles:
        title = article[0]
        link = article[1]
        source = article[2]
        date = article[3]

        # Check if an article with a similar title has been shared in the last 24 hours
        c.execute("SELECT title FROM articles WHERE shared=1 AND date >= ?", (datetime.now() - timedelta(hours=24),))
        shared_articles = c.fetchall()
        shared = False
        for shared_article in shared_articles:
            print("Comparing:", title, "with:", shared_article[0])
            if compare_articles(title, shared_article[0]):
                shared = True
                print("NOT sharing article:", title, "because it is similar to:", shared_article[0])
                break

        # If the article hasn't been shared, share it
        if not shared:
            # Mastodon API call to share the article goes here
            toot = f"{title} | {source}\n\n{link}"
            mastodon.status_post(status=toot, visibility='unlisted')

            # Update the shared status of the article in the database
            c.execute("UPDATE articles SET shared=1 WHERE title=?", (title,))
            conn.commit()

            # Wait 1 second before sharing the next article
            time.sleep(1)

    # Commit the changes to the database and close the connection
    conn.commit()
    conn.close()

def main():
    rss_links = read_rss_links('rss_links.txt')
    conn = sqlite3.connect('articles.db')
    for rss_link in rss_links:
        articles = fetch_articles(rss_link)
        save_articles(articles, conn)
    conn.close()

if __name__ == '__main__':
    main()
    share_articles()
